@extends('layouts.layout')

@section('content')

<div class="container">
    <h3>Novo cliente</h3>
    <h4>{{$cliente_type == \App\Cliente::TYPE_INDIVIDUAL? 'Pessoa Física': 'Pessoa Jurídica'}}</h4>
    <a href="{{route('clientes.create',['cliente_type' => \App\Cliente::TYPE_INDIVIDUAL])}}">Pessoa Física</a> |
    <a href="{{route('clientes.create',['cliente_type' => \App\Cliente::TYPE_LEGAL])}}">Pessoa Jurídica</a>
</div>
    
   @include('admin.form._form_errors')
    
    <div class="container">
        <form method="post" action="{{ route('clientes.store') }}">
            @include('admin.clientes._form')
            <button type="submit" class="btn btn-default">Criar</button>
         </form>
    </div>
    
@endsection