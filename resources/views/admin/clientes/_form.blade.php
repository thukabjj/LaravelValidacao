@csrf
<input type="hidden" name="cliente_type" value="{{$cliente_type}}">
<div class="form-group">
    <label for="name">Nome</label>
    <input class="form-control" id="name" name="name" value="{{old('name',$cliente->name)}}">
</div>

<div class="form-group">
    <label for="document_number">Documento</label>
    <input class="form-control" id="document_number" name="document_number" value="{{old('document_number',$cliente->document_number)}}">
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    <input class="form-control" id="email" name="email" type="email" value="{{old('email',$cliente->email)}}">
</div>

<div class="form-group">
    <label for="phone">Telefone</label>
    <input class="form-control" id="phone" name="phone" value="{{old('phone',$cliente->phone)}}">
</div>
@if($cliente_type == \App\Cliente::TYPE_INDIVIDUAL)
    @php
        $marital_status = $cliente->marital_status;

    @endphp

    <div class="form-group">
        <label for="marital_status">Estado Civil</label>
        <select class="form-control" name="marital_status" id="marital_status" value="{{$marital_status}}">
            <option value="">Selecione o estado civil</option>
            <option value="1" {{old('marital_status',$marital_status) == 1?'selected="selected"':''}}>Solteiro</option>
            <option value="2" {{old('marital_status',$marital_status) == 2?'selected="selected"':''}}>Casado</option>
            <option value="3" {{old('marital_status',$marital_status) == 3?'selected="selected"':''}}>Divorciado
            </option>
        </select>
    </div>
        <div class="form-group">
            <label for="date_birth">Data Nasc.</label>
            <input class="form-control" id="date_birth" name="date_birth" type="date" value="{{old('date_birth',$cliente->date_birth)}}">
        </div>
        @php
            $sex  =$cliente->sex;
        @endphp
        <div class="radio">
            <label>
                <input type="radio" name="sex" value="m" {{old('sex',$sex) == 'm'?'checked="checked"':''}}> Masculino
            </label>
        </div>

        <div class="radio">
            <label>
                <input type="radio" name="sex" value="f" {{old('sex',$sex) == 'f'?'checked="checked"':''}}> Feminino
            </label>
        </div>

        <div class="form-group">
            <label for="physical_disability">Deficiência Física</label>
            <input class="form-control" id="physical_disability" name="physical_disability" value="{{old('physical_disability',$cliente->physical_disability)}}">
        </div>   
@else
    <div class="form-group">
        <label for="company_name">Nome Fantasia</label>
        <input class="form-control" id="company_name" name="company_name" value="{{old('company_name',$cliente->company_name)}}">
    </div>
@endif
<div class="checkbox">
    <label>
        <input id="defaulter" name="defaulter" type="checkbox" {{old('defaulter',$cliente->defaulter)?'checked="checked"':''}}>
        Inadimplente?
    </label>
</div>