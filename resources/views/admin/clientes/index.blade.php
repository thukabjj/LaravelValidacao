@extends('layouts.layout')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md text-center">
            <h3>Listagem de clientes {{$clientes->total()}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md text-center">
            <a class="btn btn-primary" href="{{ route('clientes.create') }}">Criar novo</a>
        </div>
    </div>
</div>
<div class="container-fluid mt-2">
    <form action="" method="get">
        <input type="search" name="pesquisa" value="{{@$_GET['pesquisa']}}">
        <button type="submit">Buscar</button>
    </form>
    <div class="row">
        {{$clientes->appends(['busca' => @$_GET['busca'], 'pesquisa'=> @$_GET['pesquisa']])->links()}}
            <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>CNPJ/CPF</th>
                        <th>Data Nasc.</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Sexo</th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clientes as $client)
                        <tr>
                            <td>{{ $client->id }}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ $client->document_number }}</td>
                            <td>{{ $client->date_birth }}</td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->phone }}</td>
                            <td>{{ $client->sex }}</td>
                            <td>
                                <a href="{{route('clientes.edit',['client' => $client->id])}}">Editar</a> |
                                <a href="{{route('clientes.show',['client' => $client->id])}}">Ver</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endsection
            
    </div>
</div>
    
    