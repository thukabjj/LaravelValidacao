@extends('layouts.layout')

@section('content')
    <h3>Ver cliente</h3>
    <a class="btn btn-primary" href="{{ route('clientes.edit',['cliente' => $cliente->id]) }}">Editar</a>
    <a class="btn btn-danger" href="{{ route('clientes.destroy',['cliente' => $cliente->id]) }}"
        onclick="event.preventDefault();
        if(confirm('Deseja excluir este item?')){document.getElementById('form-delete').submit();}">Excluir</a>
    <form id="form-delete"style="display: none" action="{{ route('clientes.destroy',['cliente' => $cliente->id]) }}" method="post">
        {{csrf_field()}}
        {{method_field('DELETE')}}
    </form>
    <br/><br/>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <th scope="row">ID</th>
            <td>{{$cliente->id}}</td>
        </tr>
        <tr>
            <th scope="row">Nome</th>
            <td>{{$cliente->name}}</td>
        </tr>
        <tr>
            <th scope="row">Documento</th>
            <td>{{$cliente->document_number}}</td>
        </tr>
        <tr>
            <th scope="row">E-mail</th>
            <td>{{$cliente->email}}</td>
        </tr>
        <tr>
            <th scope="row">Telefone</th>
            <td>{{$cliente->phone}}</td>
        </tr>
        <tr>
            <th scope="row">Estado Civil</th>
            <td>
                @switch($cliente->marital_status)
                    @case(1)
                        Solteiro
                        @break

                    @case(2)
                        Casado
                        @break

                    @case(3)
                        Divorciado
                        @break
                @endswitch
            </td>
        </tr>
        <tr>
            <th scope="row">Data Nasc.</th>
            <td>{{$cliente->date_birth}}</td>
        </tr>
        <tr>
            <th scope="row">Sexo</th>
            <td>{{$cliente->sex == 'm' ? 'Masculino': 'Feminino'}}</td>
        </tr>
        <tr>
            <th scope="row">Def. Física</th>
            <td>{{$cliente->physical_disability}}</td>
        </tr>
        <tr>
            <th scope="row">Inadimplente</th>
            <td>{{$cliente->defaulter?'Sim': 'Não'}}</td>
        </tr>
        </tbody>
    </table>
@endsection