@extends('layouts.layout')

@section('content')
<div class="container">
    <h3>Editar cliente</h3>
    @include('admin.form._form_errors')
    <form method="post" action="{{ route('clientes.update',['cliente' => $cliente->id]) }}">
        <input type="hidden" name="id" value="{{$cliente->id}}">
        {{method_field('PUT')}}
            @include('admin.clientes._form')
        <button type="submit" class="btn btn-default">Salvar</button>
    </form>
@endsection
</div>
   