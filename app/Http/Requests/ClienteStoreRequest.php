<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use \App\Rules\ValidaCPFCNPJ;
use App\Cliente;

class ClienteStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       // Descobrir uma maneira de pegar um atributo do request
       
        
        $cliente_type = Cliente::getClientType($this->cliente_type);
        $rules = [
            'name'               => 'required|max:255',
            'email'              => 'required|email',
            'phone'              =>'required'
        ];

        $maritalStatus = implode(',',array_keys(Cliente::MARITAL_STATUS));
        $rulesIndivudal = [
            'date_birth'         =>'required|date',
            'document_number'    => "required|cpf|unique:clientes,document_number,".$this->id,
            'marital_status'     =>"required|in:$maritalStatus",
            'sex'                =>'required|in:m,f',
            'physical_disability'=>'max:255'
        ];

        $rulesLegal = [
            'company_name' => 'required|max:255',
            'document_number'    => "required|cnpj|unique:clientes,document_number,".$this->id,
        ];

        if($cliente_type == Cliente::TYPE_INDIVIDUAL){
            return array_merge($rules,$rulesIndivudal);
        }else{
            return array_merge($rules,$rulesLegal);
        }
    }
    public function messages(){
        return [
            'date_birth.required' => 'Data de Nascimento é obrigatório.',
            'document_number.unique' => 'O valor do campo de Documento já esta sendo utilizado.',
            'name.max' => 'Não ultrapasse :max caracteres'

        ];
    }
}
