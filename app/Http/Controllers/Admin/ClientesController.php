<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use \App\Http\Requests\ClienteStoreRequest;
use App\Http\Controllers\Controller;
use App\Cliente;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pesquisa = $request->input('pesquisa');
        $clients_query = \App\Cliente::where(function($q) use($pesquisa){
            $q->where('name', 'like', '%'.$pesquisa.'%');
            $q->orWhere('email', 'like', '%'.$pesquisa.'%');
        });
        $clients = $clients_query->paginate(2);
        return view('admin.clientes.index',['clientes'=>$clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      
        
        return view('admin.clientes.create',['cliente'=>new Cliente(),'cliente_type' =>Cliente::getClientType($request->cliente_type)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( ClienteStoreRequest $request)
    {
        $data = $request->all();
        $data['defaulter'] = $request->has('defaulter');
        $data['cliente_type'] = Cliente::getClientType($request->cliente_type);
        Cliente::create($data);
        return redirect()->route('clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        return view('admin.clientes.show',['cliente'=>$cliente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        $cliente = Cliente::findOrFail($cliente->id);
        
        return view('admin.clientes.edit',['cliente' =>$cliente,'cliente_type' =>Cliente::getClientType($cliente->cliente_type)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteStoreRequest $request, Cliente $cliente)
    {
        
        $cliente = Cliente::findOrFail($cliente->id);
        $data = $request->all();
        $data['defaulter'] = $request->has('defaulter');
        $cliente->fill($data);
        $cliente->save();
        return redirect()->route('clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return redirect()->route('clientes.index');
    }

}
