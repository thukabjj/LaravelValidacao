<?php

namespace App\Notifications;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Messages\MailMessage;
use \Illuminate\Auth\Notifications\ResetPassword;
use \Illuminate\Support\Facades\Auth;

class MyResetPasswordNotification extends ResetPassword
{
    
    public function toMail($notifiable)
    {   
        return (new MailMessage)
        ->greeting('Olá! '.$notifiable->name)
        ->subject(Lang::getFromJson('Redefinição de Senha'))
        ->line(Lang::getFromJson('Você esta recebendo esta mensagem, por que uma requisição de redefinação de senha foi chamada ...'))
        ->action(Lang::getFromJson('Redefinir Senha'), url(config('app.url').route('password.reset', $this->token, false)))
        ->line(Lang::getFromJson('Se não requisitou esta ação simplesmente ignore.'));
        }

}
