<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    const TYPE_INDIVIDUAL = 'individual';
    const TYPE_LEGAL = 'legal';
    const MARITAL_STATUS = [
        1   => 'Solteiro',
        2   => 'Casado',
        3   => 'Divorciado'
    ];
    protected  $fillable = [
        'name',
        'document_number',
        'email',
        'phone',
        'defaulter',
        'date_birth',
        'sex',
        'marital_status',
        'company_name',
        'physical_disability',
        'cliente_type'
    ];
    public static function getClientType($type){
        return $type == Cliente::TYPE_LEGAL ? $type : Cliente::TYPE_INDIVIDUAL;
    }
}
