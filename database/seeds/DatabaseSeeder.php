<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Cliente::class,5)->state(\App\Cliente::TYPE_INDIVIDUAL)->create();
        factory(\App\Cliente::class,5)->state(\App\Cliente::TYPE_LEGAL)->create();
    }
}
