<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterToClienteTipoClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->string('document_number')->unique()->nullable()->change();
            $table->date('date_birth')->nullable()->change(); 
            \DB::statement('ALTER TABLE clientes CHANGE COLUMN sex sex CHAR NULL');
            $marital_status = array_keys(\App\Cliente::MARITAL_STATUS);
            $maritalStatusString = array_map(function($value){
                return "'$value'";
            },$marital_status);
            $maritalStatusEnum = implode(",",$maritalStatusString);
            \DB::statement("ALTER TABLE clientes CHANGE COLUMN marital_status marital_status ENUM($maritalStatusEnum) NULL");
            //$table->char('sex',1)->nullable()->change(); 
            //$table->enum('marital_status',array_keys(\App\Cliente::MARITAL_STATUS))->nullable()->change();
            $table->string('physical_disability')->nullable()->change();
            $table->string('company_name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clientes', function (Blueprint $table) {
            $table->dropUnique('clientes_document_number_unique');
            $table->date('date_birth')->change(); 
            \DB::statement('ALTER TABLE clientes CHANGE COLUMN sex sex CHAR NULL');
            $marital_status = array_keys(\App\Cliente::MARITAL_STATUS);
            $maritalStatusString = array_map(function($value){
                return "'$value'";
            },$marital_status);
            $maritalStatusEnum = implode(",",$maritalStatusString);
            \DB::statement("ALTER TABLE clientes CHANGE COLUMN marital_status marital_status ENUM($maritalStatusEnum) NOT NULL");
            //$table->char('sex',1)->change(); 
            //$table->enum('marital_status',array_keys(\App\Cliente::MARITAL_STATUS))->change();
            $table->string('physical_disability')->change();
            $table->string('company_name')->change();
        
        });
    }
}
